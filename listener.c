#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT "8888" // Port to connect to

/*
 * REFERENCES
 * http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#simpleclient
 * http://beej.us/guide/bgipc/output/html/multipage/unixsock.html
 * http://beej.us/guide/bgnet/output/print/bgnet_A4.pdf
 * http://www.binarytides.com/multiple-socket-connections-fdset-select-linux/
 * http://stackoverflow.com/questions/14388706/socket-options-so-reuseaddr-and-so-reuseport-how-do-they-differ-do-they-mean-t/14388707#14388707
 * http://stackoverflow.com/questions/6856635/hide-password-input-on-terminal
 */

int main(int argc, char* argv[])
{
	// Check for arguments.
	(void)argv;
	if (argc != 1) {
		fprintf(stderr,"Arguments not required.\n");
		return(1);
	}

	// Size of buffer
	const int sz = 256;

	// Used for acquiring/binding sockets
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	// Retrieve address used for sockets
	int rv = 0;
	struct addrinfo *servinfo, *p;
	if ((rv = getaddrinfo("127.0.0.1", PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return(1);
	}

	int sockfd = 0;
	// Connect to first socket availiable
	for(p = servinfo; p != NULL; p = p->ai_next)
	{
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}

	// Check for connection
	if (p == NULL) {
		freeaddrinfo(servinfo);
		fprintf(stderr, "Failed to connect.\n");
		return(1);
	}

	// No longer needed
	freeaddrinfo(servinfo);

	// Turn echoing off
    struct termios oldt, newt;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

	// Initialize variables used when dealing with FDs
	char* buf = malloc(sizeof(*buf) * sz);
	int numBytes = 0;

	// Main loop
	while(1)
	{
		// Recieve data from socket
		if ((numBytes = recv(sockfd, buf, sz-1, 0)) > 0) {
			buf[numBytes] = '\0';
			printf("Received: %s\n", buf);
		}
		// No data sent = disconnect
		else if( numBytes == 0)
		{
			printf("Server disconnected.\n");
			break;
		}
		// Negative return value denotes error.
		else
		{
			perror("recv");
			break;
		}
	}

	// Close out program
	free(buf);
	close(sockfd);
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	return(0);
}