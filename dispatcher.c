#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>


#define PORT "8888" // Port to connect to

/*
 * REFERENCES
 * http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#simpleclient
 * http://beej.us/guide/bgipc/output/html/multipage/unixsock.html
 * http://beej.us/guide/bgnet/output/print/bgnet_A4.pdf
 * http://www.binarytides.com/multiple-socket-connections-fdset-select-linux/
 * http://stackoverflow.com/questions/14388706/socket-options-so-reuseaddr-and-so-reuseport-how-do-they-differ-do-they-mean-t/14388707#14388707
 * http://stackoverflow.com/questions/6856635/hide-password-input-on-terminal
 */

/*
 * NOTES
 * End of Transmission character is a '!'
 */

int main(int argc, char* argv[])
{
	// Check for arguments.
	(void)argv;
	if (argc != 1) {
		fprintf(stderr,"Arguments not required.\n");
		return(1);
	}

	// Create master and temp FD sets and zero
	fd_set master;
	fd_set read_fds;
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	// Size of buffer
	const int sz = 256;

	// FD to be used for accepting connections
	int listener;

	// Used for acquiring/binding sockets
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	// Retrieve address used for sockets
	int rv = 0;
	struct addrinfo *ai;
	if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
		fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
		return(1);
	}

	// Bind a socket
	struct addrinfo *p;
	int yes=1; // For setsockopt()
	for(p = ai; p != NULL; p = p->ai_next)
	{
		listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (listener < 0) {
			continue;
		}

		// Allow same address to be used
		setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

		if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
			close(listener);
			continue;
		}
		break;
	}

	if (p == NULL) {
		// Error binding
		fprintf(stderr, "Fail to bind\n");
		return(1);
	}

	// ai is no longer needed
	freeaddrinfo(ai);

	// Listen on socket
	if (listen(listener, 10) == -1) {
		perror("listen");
		return(3);
	}
	// add the listener to the master set
	FD_SET(listener, &master);
	FD_SET(STDIN_FILENO, &master);

	// Update the highest FD
	int fdmax = listener;

	// Initialize variables used when dealing with FDs
	struct sockaddr_storage remoteaddr;
	socklen_t addrlen = sizeof(remoteaddr);
	char* buf = malloc(sizeof(*buf) * sz);

	// Main loop
	while(1)
	{
		// Copy the master FDs'
		read_fds = master;

		// Wait until recieve at least one connection
		if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
			perror("select");
			return(1);
		}

		// Loop through open FDs
		for(int i = 0; i <= fdmax; i++) {
			// Check FDs for anything new
			if( FD_ISSET(i, &read_fds) )
			{
				// New connection recieved
				if( i == listener )
				{
					int newfd = accept(listener, (struct sockaddr *)&remoteaddr, &addrlen);
					if (newfd == -1) {
						perror("accept");
					}
					else {
						printf ("Listener connected.\n");

						// Add new FD to master set
						FD_SET(newfd, &master);

						// Check for new max FD
						if (newfd > fdmax) {
							fdmax = newfd;
						}
					}
				}
				// Stdin has data
				else if( i == STDIN_FILENO)
				{
					int numBytes = 0;
					if ((numBytes = read(i, buf, sz)) > 0) {
						if(buf[0] == '!') {
							printf("End of transmission character entered.\n");
							free(buf);
							return(0);
						}
						buf[numBytes-1] = '\0';
						printf ("Inputted: %s\n", buf);
						for (int idx = 0; idx <= fdmax; idx++)
						{
							if (FD_ISSET (idx, &master) && idx != STDIN_FILENO && idx != listener) {
								if (send (idx, buf, numBytes, 0) == -1) {
									perror ("send");
								}
							}
						}
						printf("Sent message to listeners.\n");
					}
				}
				// Listener sent something
				else {
					int numBytes = 0;
					if( (numBytes = recv(i, buf, sz, 0)) <= 0 )
					{
						// Error or connection closed
						if (numBytes == 0) {
							printf ("Listener disconnected.\n\n");
						}
						else {
							perror ("recv");
						}

						// Close and remove FD
						close (i);
						FD_CLR (i, &master);
					}
				}
			}
		}
	}

	free(buf);
	return(0);
}