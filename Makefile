
CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline
CFLAGS+=-D _XOPEN_SOURCE=500
CFLAGS+=-D _POSIX_C_SOURCE=200809L

all: dispatcher listener

dispatcher:

listener:

.PHONY: clean debug

clean:
	-rm dispatcher listener *.o

debug: CFLAGS+=-g
debug: dispatcher
debug: listener